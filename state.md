#### 使用说明
---

注册组件后直接使用，支持自定义样式，高度自定义。

###### 使用方式

```
import zhTabs from '@/components/zh-Tabs/zhTabs/zhTabs.vue'
```
###### 在页面中使用组件
```
import zhTabs from '@/components/zh-Tabs/zhTabs/zhTabs.vue'

<zhTabs :list='list' :current.sync='current' bgColor='#EBEBEB' round='50' height='60'
	:activeStyle="{color:'#fff'}" @click="clickZhTabs()">
</zhTabs>

```
#### 参数说明
---

| 参数			| 类型	| 默认值		| 描述							|
| ---			| ---	| ---		| ---							|
| list			| Array	| []		| 菜单数据						|
| keyName		| String| name		| 从list元素对象中读取的键名		|
| current		| Number| 0			| 当前选中标签的索引				|
| duration		| Number| 250		| 滑块移动一次所需的时间，单位ms	|
| bgColor		| String| '#EBEBEB'	| 菜单背景颜色					|
| round			| Number| 40		| 菜单圆角						|
| markBgColor	| String| '#5CC745'	| 选择中滑块的背景颜色			|
| activeStyle	| Object| {}		| 选择中时的样式					|
| inactiveStyle	| Object| {}		| 非选中时的样式					|


##### 示例

```
<template>
	<view class="">
		<view class="title">滑动菜单，自动计算滑块宽度，自动居中</view>
		<view class="menu">
			<zhTabs :list='list' :current.sync='current' bgColor='#EBEBEB' round='50' height='60'
				:activeStyle="{color:'#fff'}" @click="clickZhTabs()">
			</zhTabs>
		</view>
	</view>
</template>

<script>
	import zhTabs from '@/components/zh-Tabs/zhTabs/zhTabs.vue'
	export default {
		components: {
			zhTabs
		},
		data() {
			return {
				list: [{
						id: 1,
						name: '苹果'
					},
					{
						id: 2,
						name: '香蕉'
					},
					{
						id: 3,
						name: '西瓜'
					},
					{
						id: 4,
						name: '菠萝'
					},
					{
						id: 5,
						name: '石榴'
					},
					{
						id: 6,
						name: '榴莲'
					},
					{
						id: 7,
						name: '香橙'
					},
					{
						id: 8,
						name: '黄桃'
					}
				],
				current: 0,
			}
		},
		methods: {
			clickZhTabs(e) { //点击zhTabs
				console.log(e);
			}
		},
	}
</script>

<style lang="scss" scoped>
	.title {
		margin-top: 40upx;
		color: #909090;
		font-size: 30rpx;
		text-align: center;
	}

	.menu {
		margin-top: 40upx;
		padding: 0 28upx;
	}
</style>

```



